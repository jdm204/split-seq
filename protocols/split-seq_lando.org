#+title: Split seq wet lab protocol
#+author: David Lando, Chris Steel 

* Reagent list

** Fixation 

*Solutions for one sample*

*PBS+RI* make up fresh and store on ice
- To 2ml PBS (TC grade) add 25 ul RNase OUT (40U/ul) and 50ul SUPERase inhibitor (20U/ul)

*PBS+1.33% formaldehyde* make up fresh on the day
- To 2.75 ml PBS (TC grade) add 0.25 ml 16% formaldehyde (Pierce cat.28906)

** Split barcoding

*RT primer mixes*
Philpot protocol has 12 barcodes (R01 - R12) (two for each, one oligo dT, one random hexamer)

We will add the RNA barcodes r01 r02.... etc, each in eppindorf tube (for each sample):
- 12.5ul RNA_RE#1 to #12 (100uM)
- 12.5ul RNA_NRE_#1 to #12 (100uM), matched with corresponding RNA_RE_#
- 75ul nuclease free water, e.g. Ambion Am9937
- Mix and store -20^oC

*PBS+PI* Make up fresh on the day and store on ice
- To 10ml of PBS (TC grade) add 1 tablet cOmplete EDTA-free PI Cocktail (Roche, 11873580001).
- Mix/vortex, one dissolved store on ice.

*PBS+RI with protease inhibitors* make up fresh and store on ice
- To 4000 ul 1xPBS+PI add 50ul RNAse OUT (40U/ul) and 100 ul SUPERase inhibitor (20U/ul) - enough for 12 samples

*1x NEBuffer 3.1 make up fresh on the day*
- Add 300ul of 10x stock to 2.7 mls of nucelase free MQW (Ambion AM9937) and store on ice.

*BSA coated tubes* for 12 samples make 10 tubes
- Use Axygen Maxymum recovery (MCT-150-L-C) or Eppendorf low protein bind 1.5ml tubes. Add 1.5ml 0.5% BSA (0.1g BSA (Sigma A3059) in 20 ml nuclease free water) to tubes, leave at RT for 10min, pipet to remove BSA, brief spin, remove last drop of BSA. No need to air dry. Store in fridge for up to 1 week.

#+CAPTION:PBS-Triton+RI with protease inhibitors make up fresh & store on ice
| Reagent      | Stock    | for 12 reactions (+extra) | Note                                        |
|--------------+----------+---------------------------+---------------------------------------------|
| PBS+PI       | TC grade | 6050ul                    |                                             |
| Triton-X-100 | 5%       | 242ul                     | final conc 0.2%                             |
| Superase In  | 20U/ul   | Invitrogen AM2696, 158ul  | Add RNase inhibitors immediatley before use |
| RNaseOUT     | 40U/ul   | Invitrogen 10777019, 79ul | Add RNase inhibitors immediatley before use |

#+CAPTION: RT mix, make up fresh & store on ice
| Reagent              | Stock                      | for 12 reactions (+extra)  | Note                                        |
|----------------------+----------------------------+----------------------------+---------------------------------------------|
| RT buffer (Maxima H) | 5x, with Maxima H (EP0752) | 49.2ul                     |                                             |
| PBS                  | TC grade                   | 52.8ul                     |                                             |
| dNTP mix             | 10mM, NEB N0447S           | 13.2ul                     |                                             |
| BSA                  | 5% (50mg/ml)               | 3.7 ul                     |                                             |
| Superase In          | 20U/ul                     | Invitrogen AM2696, 3.3ul   | Add RNase inhibitors immediatley before use |
| RNaseOUT             | 40U/ul                     | Invitrogen 10777019, 1.7ul | Add RNase inhibitors immediatley before use |
*Total 123.81ul; need 9ul per reaction*

#+CAPTION: two rounds of barcoding, so you will need two tubes, Make up in two 50ml falcon tubes & store on ice.
| Reagent               |        Stock | For one 96 well plate              | Note |
|-----------------------+--------------+------------------------------------+------|
| water (nucelase free) |              | 2290 ul                            |      |
| T4 ligase buffer      |          10x | with T4 DNA ligase (M0202L), 500ul |      |
| BSA                   | 5% (50mg/ml) | Invitrogen, AM2616, 20ul           |      |
| NEBuffer 3.1          |          10x | NEB, B6003S, 100ul                 |      |
The 2910ul Ligation mix is enough for one 96 well plate ligation-based barcoding (plus extra)

#+CAPTION: R02 Blocking Solution, make up and store on ice 
| Reagent               | Stock | For one 96 well plate                         | Note |
|-----------------------+-------+-----------------------------------------------+------|
| R02 Blocker           | 100uM | desalt purified & resuspended in water, 264ul |      |
| T4 ligase buffer      | 10x   | With T4 DNA ligase (M0202L), 250ul            |      |
| water (nucelase free) |       | 486ul                                         |      |
1ml is enough for one 96 well plate (need 10ul R02 Blocking Solution per well of 96 well plate

#+CAPTION: R03 Termination Solution, Make us as needed & store on ice 
| Reagent               | Stock | For one 96 well plate                        | Note |
|-----------------------+-------+----------------------------------------------+------|
| R03 Quencher          | 100uM | desalt purified, resuspended in water, 264ul |      |
| EDTA                  | 0.5M  | 500ul                                        |      |
| water (nucelase free) |       | 236ul                                        |      |
1ml is enough for one 96 well plate (need 10ul R03 Termination Solution per well of 96 well plate)
Note: EDTA will inhibit DNA ligase.

* Protocols

** Fixation

Need approx 500K cells

- Harvest cells and resuspend pellet in 1ml PBS+RI
- Add 3ml ice cold PBS+1.33% formaldehyde and mix gentely by pipetting up and down 2-3 times
- Incubate on ice for 10 minutes
- Spin @ 300g for 3 minutes @ RT
- Wash pellet with 1ml PBS+RI and re-spin @ 300g for 3 mins @ RT
- Remove supernatent and snap freeze pellet on dry ice
- Store pellet at -80^oC

** split barcoding

*Whole cell prep*
- Thaw cell pellet on ice.
- Add 520ul PBS-Triton+RI and mix gently by pipetting up and down 2-3 times.
- Incubate for 3min on ice.
- Add 500 ul of ice cold 100 mM Tris pH8.0 and gently mix by pipetting up and down 2 times.
- Resuspend cells in 250 ul 1x PBS+RI (0.5 U/ul RNase OUT, 0.5 U/ul SUPERase Inhibitor).
- Count cells- To 1ul of PI (1mg/ml) add 9 ul of cell sample and analyse with Countess 3 FL Cell Counter (with EVOS light cube RFP 2.0). Should see nice single PI-stained cell and not any large clumps or much cell debris.
- Spin 600g for 3 min at 4˚ C. Remove supernatant and dilute to 20000 cells/ul with PBS+RI

*Reverse transcription*
- Arrange the 12 RT sample PCR tubes (individual or strip tubes) on a metal holder on ice.
- To each individual PCR tube add 5 ul cell suspension (100K cells), 4 ul of the correct RT primer mix, 2 ul Maxima H Minus Reverse Transcriptase (200 U/µL) and 9 ul of the RT mix +RIs. Resuspend the cells gently, ie. pipette up-&-down slowly about 5-6x.
- Carry out RT in a thermocycler using the following steps.

#+CAPTION: PCR conditions
| Step | temp ^oC | time    | notes    |
|------+----------+---------+----------|
|    1 |       50 | 10 min  |          |
|    2 |        8 | 12 secs |          |
|      |       15 | 45 secs | 3 cycles |
|      |       20 | 45 secs | ^        |
|      |       30 | 30 secs | ^        |
|      |       42 | 2 mins  | ^        |
|      |       50 | 3 mins  | ^        |
|    3 |       50 | 5 mins  |          |

- Combine the cell from all tubes into one BSA-coated Maxymum recovery tube on ice and mix well.
- Add 4.8ul 5% Triton X-100 and mix gently by pipetting up-&-down using a p1000.
- Spin down the cell 800g 10min 4˚ C.
- Remove & discard the supernatant and immediately proceed to the ligation-based barcoding.

*Ligation based barcoding*
- Before starting check you have: Working annealed barcode plates, one each of R02 and R03 (thawed). Two 50ml tubes, each containing 2,910ul ligation mixture, on ice. R02 Blocking Solution (1ml) and R03 Termination Solution (1ml) on ice. Make sure 96 well plate adaptor is on the thermomixer and the temperature set to 37˚ C.
- Resuspend the pelleted combined cells gently in 1ml of ice cold 1x NEBuffer 3.1 by slowly pipetting up-&-down. Check cell number using Countess 3 FL Cell Counter
- Add 1ml of cell (in Buffer NEBuffer 3.1) into the first 50ml tube containing 2910l Ligation Mixture on ice and resuspend gently but well by pipetting up-&-down a few times.
- Add 100ul T4 DNA ligase (NEB M0202L, 400,000 units/ml) to the cell on ice and mix gently but well by pipetting up-&-down.
- Open working barcode plate R02: use metal 96 well plate (no need to be cold, can be at room temp) to hold the barcode plate and carefully remove the cover film. Leave the plate on the metal holder.
- The Stepper pipette does not fit into a 50ml falcon tube, so you need to mix then pipet 1-1.5 ml of cell ligation mix into a 1.5 ml eppendorf everytime you want to fill the stepper with 1 ml of solution. This can be at RT.
- Using the Stepper, aliquot 40l of cell ligation mix into each well of the working barcode plate R02. Seal the plate carefully and completely using the StarSeal film.
- Place the plate in Thermomixer with 96 well plate adaptor. Mix plate for 30 sec at 1200rpm. Then incubate at 37˚ C, 30min at 300rpm. After incubation mix 30 sec at 1200rpm.
- Remove the plate from the thermomixer (no need to do a brief spin) and keep on metal plate (at RT) while carefully remove sealing film. Add 10l R02 Blocking. Seal the plate carefully and completely using StarSeal film.
- Place the plate in Thermomixer with 96 well plate adaptor. Mix plate for 30 sec at 1200rpm. Then incubate at 37˚ C, 30min at 300rpm. After incubation mix 30 sec at 1200rpm.
- Next you will combine all the cells from the 96 wells into 4 tubes: Remove the plate from the thermomixer and transfer to metal plate holder (at RT) and carefully remove sealing film. Aliquot the cells between four pre-chilled BSA-coated Maximum recovery tubes on ice, ie. cells from 24 wells (2 rows) per tube.
- Spin down 1000g, 10min, 4˚ C.
- Carefully remove & discard the s/n and add 100ul 1x NEBuffer 3.1 to each of the four tubes. Gently but completely resuspend the cell, add all to one tube and make up to 1ml with 1x NEBuffer 3.1 (NEB, B6003S). Check cell number using Countess 3 FL Cell Counter
- Add 1ml of cells (in NEBuffer 3.1) into the second 50ml tube containing 2910ul Ligation Mixture on ice and resuspend gently but well by pipetting up-&-down.
- Add 100ul T4 DNA ligase to the cells on ice and mix gently but well by pipetting up-&-down.
- Open working barcode plate R03: use metal 96well plate (no need to be cold can be at room temp) to hold the barcode plate and carefully remove the cover film. Leave the plate on the metal holder.
- As before, pipet 1-1.5 ml of cell ligation mix into a 1.5 ml eppendorf everytime you want to fill the stepper with 1 ml of solution. Using the Stepper, aliquot 40l of cell ligation mix into each well of the barcode plate R03.
- Seal the plate carefully and completely using StarSeal film.
- Place the plate in Thermomixer with 96 well plate adaptor. Mix plate for 30 sec at 1200rpm. Then incubate at 37˚ C, 30min at 300rpm. After incubation mix 30 sec at 1200rpm.
- Remove the plate from the thermomixer and keep on metal plate (at RT) while carefully remove sealing film. Add 10ul R03 Termination Solution to each well using the stepper.
- Place the plate in Thermomixer and mix for 30 sec at 1200rpm.
- Next combine all the cells from the 96 wells. Aliquot the cells between four pre-chilled BSA-coated Maximum recovery tubes on ice, ie. cells from 24 wells (2 rows) per tube.
- Spin down 1000g, 10min, 4˚ C.
- Combine the cells into one tube: carefully remove & discard the s/n from each tube on ice and add 100l ice cold PBS to each of the four tubes. Gently but completely resuspend the cells and combine all into one tube.
- Need to check the recovered cells for clumping and get an accurate count of single cells for later aliquoting of the sub-libraries. Resuspend the cells using P1000 up-&-down 2-3x, then take 9l of the cell suspension and add to 1l of 1mg/ml PI. Using Countess 3 FL Cell Counter (with EVOS light cube RFP 2.0), count the PI-stained cell.
- Spin down cell 1000g, 10min, 4˚ C.
- Resuspend the cell pellet in ice-cold PBS so that 10ul = 3,000 cells.
- Aliquot into 10 ul sub-libraries containing 3,000 cells (can use regular PCR tubes; you don’t need to use maximum recovery tubes) on ice.
- Store sub-libraries at -80˚ C.


* Lab website protocol

** Important notes and plate barcode preparation

*SPLiT-seq Protocol, Version 3.0*
- Projected Experimental Time: 2 Days Recommended time on day 1 to start: morning

*Addition of RNase inhibitor to buffers:*
- When any buffer has “+RI” next to it, this indicates that enzymatics RNase inhibitor should be added to a final concentration of 0.1 U/uL.

*Centrifugation Steps:*
- All centrifugation steps should be performed with a swinging bucket rotor. Using a fixed angle centrifuge may lead to more cell loss. Depending on the tissue type, centrifugation speeds may need to be changed to optimize cell retention (e.g. smaller cells = higher speeds).

** DNA Barcoding Plate Generation (annealing oligos)

What you need:
- Three 96 well plates from IDT - Reverse Transcription Barcode Primers, Ligation Round 1,and Ligation Round 2 Stock DNA Oligo plates (100 uM)
-Two linker oligos - BC_0215, BC_0060 (Note: these are assumed to be in stock concentration of 1mM, be sure to correct for volume if only have 100 uM stocks)
-Six 96 well PCR plates (3 stock plates that will last at least 10 experiments, and 3 plates for 1st experiment) 

/Note: This will generate 100 uL of DNA barcodes for each well. Each SPLiT-seq experiment requires only 4 uL/well of the reverse transcription primer solution which will last for 25 experiments. Each SPLiT-seq experiment requires only 10 uL/well of the barcode/linker solutions, so these plates will last a total of 10 experiments./

*** Round 1 reverse transcription barcoded primers (final concentrations of 12.5 uM random hexamer and 12.5 uM 15dT primers in each of 48 wells)

1.Using multichannel pipette, add 12.5 uL of rows A-D in the IDT Reverse Transcription Barcode Primers to rows A-D of the BC Stock 96 well PCR plate.
2.Using multichannel pipette, add 12.5 uL of rows E-H in the IDT Reverse Transcription Barcode Primers to rows A-D of the BC stock 96 well PCR plate (mixing polydT with random hexamer primer here)
3.Add 75ul of water to rows A-D of the BC stock 96 well PCR plate.

*** Round 2 ligation round (Final concentrations of 12uM barcodes, 11uM linker-BC_0215)

1.Using multichannel pipette, add 12uL of IDT Round 2 Barcodes to R1 Stock 96 well PCR plate
2.Add 138.6ul of BC_0215(1mM) to 10.9494mL water in a basin (BC_0215_dil)
3.Using multichannel pipette, add 88uL BC_0215_dil to each well of R2 Stock 96 well PCR plate

*** Ligation Round 3 (Final concentrations of 14uM barcodes, 13uM linker-BC_0060)

1.Using multichannel pipette, add 14uL of Round 3 Barcodes to R3 Stock 96 well PCR plate
2.Add 163.8ul of BC_0060(1mM) to 10.6722mL water in a basin (BC_0060_dil)
3.Using multichannel pipette, add 86uL BC_0060 to each well R3 Stock 96 well PCR plate


For each ligation plate (R2 and R3, not including reverse transcription barcodes), anneal the barcode and linker oligos with the following thermocycling protocol:
1. Heat to 95C for 2 minutes
2. Ramp down to 20C for at a rate of -0.1C/s
3. 4C

Aliquot out 10 uL of each barcode/linker stock plate into 3 new 96 well PCR plates. These are the plates that should be used for DNA barcoding in the split-pool ligation steps in the protocol.

** Fixing and permeablisation

1. Prepare the following buffers (calculated for two experiments):
- A 1.33% formalin (360 uL of 37% formaldehyde solution (Sigma)+ 9.66 ml PBS) solution and store at 4C.
- 6 mL of 1X PBS+RI (15 uL of SUPERase In and 7.5 uL of enzymatics rnase inhibitor)
- 2 mL of 0.5X PBS+RI (5 uL of SUPERase in and 2.5 of enzymatics rnase inhibitor)
- 500uL of 5% Triton X-100 + RI (2 uL of SUPERase In)
- 500uL of 100mM Tris pH 8.0 + 2 uL SUPERase In
- Set the centrifuge to 4C

2. Pellet cells by centrifuging at 500g for 3 mins at 4C. (Some cells may require faster centrifugation.)
3. Resuspend cells in 1mL of cold PBS+RI. Keep cells on ice between these steps.
4. Pass cells through a 40um strainer into a fresh 15mL Falcon tube and place on ice.
   - Note: The cell resuspension is not likely to passively go through the strainer, which can cause cell loss.
   - Instead, with a 1ml pipette filled with the resuspension, press the end of the tip directly onto the strainer and actively push the liquid through. The motion should take ~1 second
5. Add 3 mL of cold 1.33% formaldehyde (final concentration of 1% formaldehyde). Fix cells on ice for 10 mins.
6. Add 160uL of 5% Triton-X100+RI to fixed cells and mix by gently pipetting up and down 5x with a 1mL pipette. Permeabilize cells for 3 mins on ice.
7. Centrifuge cells at 500g for 3 mins at 4C.
8. Aspirate carefully and resuspend cells in 500 uL of cold PBS+RI.
9. Add 500uL of cold 100 mM Tris-HCl, pH 8.0.
10. Add 20 uL of 5% Triton X-100.
11. Centrifuge cells at 500g for 3 mins at 4C.
12. Aspirate and resuspend cells in 300 ul of cold 0.5x PBS+RI.
13. Run cells through a 40uM strainer into a new 1.7mL tube.
14. Count cells using a hemacytometer or a flow-cytometer and dilute the cell suspension to 1,000,000 cells/mL. While counting cells, keep cell suspension on ice.
 
/Note: This step will dictate how many cells enter the split-pool rounds. It will be possible to sequence only a subset of the cells that enter the split-pool rounds (can be done during sublibrary generation at lysis step). The total number of barcode combinations you will be using should be calculated to determine the maximum number of cells you can sequence with minimal barcode collisions. As a rule of thumb, the number of cells you process should not exceed more than 5% of total barcode combinations. We usually have a dilution between 500k to 1M cells/mL here (equates to 4-8k cells going into each well for reverse transcription barcoding rounds)./

** Reverse transcription

1. Aliquot out 4 uL of the RT barcodes stock plate into the top 4 rows (48 wells) of a new 96 well plate. Cover the this plate with an adhesive plate seal until ready for use.

2. Create the following reverse transcription (RT) mix on ice:

 | Reagent                              | Stock           | Desired       | Per Reaction |    Volume in Mix |
 |                                      | Concentration   | Concentration |              | (48 wells + 10%) |
 |--------------------------------------+-----------------+---------------+--------------+------------------|
 | 5x RT buffer                         | 5x              | 1x            |            4 |            211.2 |
 | Enzymatics Rnase Inhibitor           | 40u/uL          | 0.25u/uL      |        0.125 |              6.6 |
 | Superase In Rnase Inhibitor          | 20U/uL          | 0.25U/uL      |         0.25 |             13.2 |
 | dNTPS                                | 10mM (per base) | 500uM         |            1 |             52.8 |
 | Maxima H Minus Reverse Transcriptase | 200u/uL         | 20u/ul        |            2 |            105.6 |
 | H20                                  | NA              | NA            |        0.625 |               33 |
 | Total Volume                         |                 |               |            8 |            422.4 |

3. Add 8uL of the RT mix to each of the top 48 wells. Each well should now contain a volume of 12uL.
4. Add 8uL of cells in 0.5x PBS+RI to each of the top 48 wells. Each well should now contain a volume of 20uL.
5. Add the plate into a thermocycler with the following protocol:
   a .50 C for 10 minutes
   b.Cycle 3 times:
                  i. 8C for 12s
		  ii. 15C for 45s
		  iii. 20C for 45s
		  iv. 30C for 30s
		  v. 42C for 2 min
		  vi. 50C for 3 min
   c.50C for 5 min
   d.4C forever
6. Place the RT plate on ice.
7. Prepare 2 mL of 1x NEB buffer 3.1 with 20uL of Enzymatics RNase Inhibitor.
8. Transfer each RT reaction to a 15mL falcon tube (also on ice).
9. Add 9.6uL of 10% Triton-X100 to get a final concentration of 0.1%.
10. Centrifuge pooled RT reaction for 3 min at 500G.
11. Aspirate supernatant and resuspend into 2 mL of 1x NEB buffer 3.1 + 20uL Enzymatics RNase Inhibitor.

** Ligation barcoding

